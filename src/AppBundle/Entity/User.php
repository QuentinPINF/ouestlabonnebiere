<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_8D93D64992FC23A8", columns={"username_canonical"}), @ORM\UniqueConstraint(name="UNIQ_8D93D649A0D96FBF", columns={"email_canonical"})})
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BeerRatings", mappedBy="user")
     */
    private $beerRatings;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ShopRatings", mappedBy="user")
     */
    private $shopRatings;

    /**
     * Constructor
     */
    public function __construct()
    {
		parent::__construct();
        $this->beerRatings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->shopRatings = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add beerRating
     *
     * @param \AppBundle\Entity\BeerRatings $beerRating
     *
     * @return User
     */
    public function addBeerRating(\AppBundle\Entity\BeerRatings $beerRating)
    {
        $this->beerRatings[] = $beerRating;

        return $this;
    }

    /**
     * Remove beerRating
     *
     * @param \AppBundle\Entity\BeerRatings $beerRating
     */
    public function removeBeerRating(\AppBundle\Entity\BeerRatings $beerRating)
    {
        $this->beerRatings->removeElement($beerRating);
    }

    /**
     * Get beerRatings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeerRatings()
    {
        return $this->beerRatings;
    }

    /**
     * Add shopRating
     *
     * @param \AppBundle\Entity\ShopRatings $shopRating
     *
     * @return User
     */
    public function addShopRating(\AppBundle\Entity\ShopRatings $shopRating)
    {
        $this->shopRatings[] = $shopRating;

        return $this;
    }

    /**
     * Remove shopRating
     *
     * @param \AppBundle\Entity\ShopRatings $shopRating
     */
    public function removeShopRating(\AppBundle\Entity\ShopRatings $shopRating)
    {
        $this->shopRatings->removeElement($shopRating);
    }

    /**
     * Get shopRatings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShopRatings()
    {
        return $this->shopRatings;
    }
}
