<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BeerPrices
 *
 * @ORM\Table(name="sells", indexes={@ORM\Index(name="FK_BeerPrices_beer_id", columns={"beer_id"}), @ORM\Index(name="FK_BeerPrices_shop_id", columns={"shop_id"})})
 * @ORM\Entity
 */
class BeerPrices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="beer_id", type="bigint")
     */
    private $beer_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="bigint")
     */
    private $shop_id;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=false)
     */
    private $price;
	
    /**
     * @var float
     *
     * @ORM\Column(name="volume", type="float", precision=10, scale=0, nullable=false)
     */
    private $volume;
	
    /**
     * @var boolean
     *
     * @ORM\Column(name="draught", type="boolean", nullable=false)
     */
    private $draught;
	
    /**
     * @var float
     *
     * @ORM\Column(name="deposit", type="float", precision=10, scale=0, nullable=false)
     */
    private $deposit;

    /**
     * @var \AppBundle\Entity\Beer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Beer", inversedBy="beer_id")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="beer_id", referencedColumnName="id")
     * })
     */
    private $beer;

    /**
     * @var \AppBundle\Entity\Shop
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Shop", inversedBy="shop_id")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     * })
     */
    private $shop;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return BeerPrices
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set beerId
     *
     * @param integer $beerId
     *
     * @return BeerPrices
     */
    public function setBeerId($beerId)
    {
        $this->beer_id = $beerId;

        return $this;
    }

    /**
     * Get beerId
     *
     * @return integer
     */
    public function getBeerId()
    {
        return $this->beer_id;
    }

    /**
     * Set shopId
     *
     * @param integer $shopId
     *
     * @return BeerPrices
     */
    public function setShopId($shopId)
    {
        $this->shop_id = $shopId;
        return $this;
    }

    /**
     * Get shopId
     *
     * @return integer
     */
    public function getShopId()
    {
        return $this->shop_id;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return BeerPrices
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set volume
     *
     * @param float $volume
     *
     * @return BeerPrices
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return float
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set draught
     *
     * @param boolean $draught
     *
     * @return BeerPrices
     */
    public function setDraught($draught)
    {
        $this->draught = $draught;

        return $this;
    }

    /**
     * Get draught
     *
     * @return boolean
     */
    public function getDraught()
    {
        return $this->draught;
    }

    /**
     * Set deposit
     *
     * @param float $deposit
     *
     * @return BeerPrices
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Get deposit
     *
     * @return float
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Set beer
     *
     * @param \AppBundle\Entity\Beer $beer
     *
     * @return BeerPrices
     */
    public function setBeer(\AppBundle\Entity\Beer $beer = null)
    {
        $this->beer = $beer;
		$this->beer_id = $beer->getId();

        return $this;
    }

    /**
     * Get beer
     *
     * @return \AppBundle\Entity\Beer
     */
    public function getBeer()
    {
        return $this->beer;
    }

    /**
     * Set shop
     *
     * @param \AppBundle\Entity\Shop $shop
     *
     * @return BeerPrices
     */
    public function setShop(\AppBundle\Entity\Shop $shop = null)
    {
        $this->shop = $shop;
		$this->shop_id = $shop->getId();

        return $this;
    }

    /**
     * Get shop
     *
     * @return \AppBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }
}
