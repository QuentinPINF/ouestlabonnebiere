<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Beer
 *
 * @ORM\Table(name="beer", indexes={@ORM\Index(name="FK_Beer_color_id", columns={"color_id"}), @ORM\Index(name="FK_Beer_brewery_id", columns={"brewery_id"})})
 * @ORM\Entity
 */
class Beer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="alcohol", type="float", precision=10, scale=0, nullable=false)
     */
    private $alcohol;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="validated", type="boolean", nullable=false)
     */
    private $validated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BeerRatings", mappedBy="beer")
     */
    private $beerRatings;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BeerPrices", mappedBy="beer")
     */
    private $beerPrices;

    /**
     * @var \AppBundle\Entity\Color
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Color")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     * })
     */
    private $color;

    /**
     * @var \AppBundle\Entity\Brewery
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Brewery")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brewery_id", referencedColumnName="id")
     * })
     */
    private $brewery;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tag", inversedBy="beers")
     * @ORM\JoinTable(name="has_tags",
     *   joinColumns={
     *     @ORM\JoinColumn(name="beer_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     *   }
     * )
     */
    private $tags;

	private $averageRating;
	
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->beerRatings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->beerPrices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Beer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set alcohol
     *
     * @param float $alcohol
     *
     * @return Beer
     */
    public function setAlcohol($alcohol)
    {
        $this->alcohol = $alcohol;

        return $this;
    }

    /**
     * Get alcohol
     *
     * @return float
     */
    public function getAlcohol()
    {
        return $this->alcohol;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Beer
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return Beer
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Add beerRating
     *
     * @param \AppBundle\Entity\BeerRatings $beerRating
     *
     * @return Beer
     */
    public function addBeerRating(\AppBundle\Entity\BeerRatings $beerRating)
    {
        $this->beerRatings[] = $beerRating;

        return $this;
    }

    /**
     * Remove beerRating
     *
     * @param \AppBundle\Entity\BeerRatings $beerRating
     */
    public function removeBeerRating(\AppBundle\Entity\BeerRatings $beerRating)
    {
        $this->beerRatings->removeElement($beerRating);
    }

    /**
     * Get beerRatings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeerRatings()
    {
        return $this->beerRatings;
    }

    /**
     * Add beerPrices
     *
     * @param \AppBundle\Entity\BeerPrices $beerPrices
     *
     * @return Beer
     */
    public function addBeerPrices(\AppBundle\Entity\BeerPrices $beerPrices)
    {
        $this->beerPrices[] = $beerPrices;

        return $this;
    }

    /**
     * Remove beerPrices
     *
     * @param \AppBundle\Entity\BeerPrices $beerPrices
     */
    public function removeBeerPrices(\AppBundle\Entity\BeerPrices $beerPrices)
    {
        $this->beerPrices->removeElement($beerPrices);
    }

    /**
     * Get beerPrices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeerPrices()
    {
        return $this->beerPrices;
    }

    /**
     * Set color
     *
     * @param \AppBundle\Entity\Color $color
     *
     * @return Beer
     */
    public function setColor(\AppBundle\Entity\Color $color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return \AppBundle\Entity\Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set brewery
     *
     * @param \AppBundle\Entity\Brewery $brewery
     *
     * @return Beer
     */
    public function setBrewery(\AppBundle\Entity\Brewery $brewery = null)
    {
        $this->brewery = $brewery;

        return $this;
    }

    /**
     * Get brewery
     *
     * @return \AppBundle\Entity\Brewery
     */
    public function getBrewery()
    {
        return $this->brewery;
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\Tag $tag
     *
     * @return Beer
     */
    public function addTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\Tag $tag
     */
    public function removeTag(\AppBundle\Entity\Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }
	
	public function setTags($tags) {
		$this->tags = $tags;
	}
	
    /**
     * Set averageRating
     *
     * @param float
     *
     * @return Beer
     */
    public function setAverageRating($averageRating)
    {
        $this->averageRating = $averageRating;
		
		return $this;
    }
	
    /**
     * Get averageRating
     *
     * @return double
     */
    public function getAverageRating()
    {
        return $this->averageRating;
    }
}
