<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BeerRatings
 *
 * @ORM\Table(name="rates_beer", indexes={@ORM\Index(name="FK_BeerRating_user_id", columns={"user_id"}), @ORM\Index(name="FK_BeerRating_beer_id", columns={"beer_id"})})
 * @ORM\Entity
 */
class BeerRatings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $user_id;

    /**
     * @var integer
     *
     * @ORM\Column(name="beer_id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $beer_id;

    /**
     * @var float
     *
     * @ORM\Column(name="rating", type="float", precision=10, scale=0, nullable=false)
     */
    private $rating;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=false)
     */
    private $comment;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="user_id")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \AppBundle\Entity\Beer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Beer", inversedBy="beer_id")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="beer_id", referencedColumnName="id")
     * })
     */
    private $beer;



    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return BeerRatings
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set beerId
     *
     * @param integer $beerId
     *
     * @return BeerRatings
     */
    public function setBeerId($beerId)
    {
        $this->beer_id = $beerId;

        return $this;
    }

    /**
     * Get beerId
     *
     * @return integer
     */
    public function getBeerId()
    {
        return $this->beer_id;
    }

    /**
     * Set rating
     *
     * @param float $rating
     *
     * @return BeerRatings
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return BeerRatings
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return BeerRatings
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return BeerRatings
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
		$this->user_id = $user->getId();

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set beer
     *
     * @param \AppBundle\Entity\Beer $beer
     *
     * @return BeerRatings
     */
    public function setBeer(\AppBundle\Entity\Beer $beer = null)
    {
        $this->beer = $beer;
		$this->beer_id = $beer->getId();

        return $this;
    }

    /**
     * Get beer
     *
     * @return \AppBundle\Entity\Beer
     */
    public function getBeer()
    {
        return $this->beer;
    }
}
