<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shop
 *
 * @ORM\Table(name="shop")
 * @ORM\Entity
 */
class Shop
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=0, nullable=false)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=200, nullable=false)
     */
    private $address;

    /**
     * @var boolean
     *
     * @ORM\Column(name="validated", type="boolean", nullable=false)
     */
    private $validated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ShopRatings", mappedBy="shop")
     */
    private $shopRatings;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BeerPrices", mappedBy="shop")
     */
    private $beerPrices;
	
	private $averageRating;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->shopRatings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->beerPrices = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Shop
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return Shop
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return Shop
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Shop
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Shop
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return Shop
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Add shopRating
     *
     * @param \AppBundle\Entity\ShopRatings $shopRating
     *
     * @return Shop
     */
    public function addShopRating(\AppBundle\Entity\ShopRatings $shopRating)
    {
        $this->shopRatings[] = $shopRating;

        return $this;
    }

    /**
     * Remove shopRating
     *
     * @param \AppBundle\Entity\ShopRatings $shopRating
     */
    public function removeShopRating(\AppBundle\Entity\ShopRatings $shopRating)
    {
        $this->shopRatings->removeElement($shopRating);
    }

    /**
     * Get shopRatings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShopRatings()
    {
        return $this->shopRatings;
    }

    /**
     * Add beerPrices
     *
     * @param \AppBundle\Entity\BeerPrices $beerPrices
     *
     * @return Shop
     */
    public function addBeerPrices(\AppBundle\Entity\BeerPrices $beerPrices)
    {
        $this->beerPrices[] = $beerPrices;

        return $this;
    }

    /**
     * Remove beerPrices
     *
     * @param \AppBundle\Entity\BeerPrices $beerPrices
     */
    public function removeBeerPrices(\AppBundle\Entity\BeerPrices $beerPrices)
    {
        $this->beerPrices->removeElement($beerPrices);
    }

    /**
     * Get beerPrices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeerPrices()
    {
        return $this->beerPrices;
    }
	
    /**
     * Set averageRating
     *
     * @param float
     *
     * @return Shop
     */
    public function setAverageRating($averageRating)
    {
        $this->averageRating = $averageRating;
		
		return $this;
    }
	
    /**
     * Get averageRating
     *
     * @return double
     */
    public function getAverageRating()
    {
        return $this->averageRating;
    }
}
