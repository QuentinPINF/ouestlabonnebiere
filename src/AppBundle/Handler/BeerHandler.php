<?php

namespace AppBundle\Handler;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class BeerHandler 
{
	private $doctrine;
	
	public function __construct($doctrine) {
		$this->doctrine = $doctrine;
	}
	
	public function getBeerAverage(&$beer) {
		$builder = $this->doctrine->getRepository("AppBundle:BeerRatings")->createQueryBuilder("b");
		$query = $builder->select("avg(b.rating)")
						->groupBy("b.beer_id")
						->where ("b.beer_id=:beer_id")
						->setParameter('beer_id', $beer->getId())
						->getQuery();
		$result = $query->getResult();
		$average = (count($result) != 0) ? $result[0][1] : 0;
		$beer->setAverageRating($average);
	}
	
	public function getBeersAverage(&$beers) {
		
		foreach($beers as $beer) {
			$this->getBeerAverage($beer);
		}
	}
}
