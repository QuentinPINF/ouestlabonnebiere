<?php

namespace AppBundle\Handler;

class BeerPricesHandler 
{
	private $doctrine;
	
	public function __construct($doctrine) {
		$this->doctrine = $doctrine;
	}
	
	public function get($id) {
		return $this->doctrine->getRepository("AppBundle:BeerPrices")->find($id);
	}
}
