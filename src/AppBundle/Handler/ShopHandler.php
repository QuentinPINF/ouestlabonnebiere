<?php

namespace AppBundle\Handler;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ShopHandler 
{
	private $doctrine;
	
	public function __construct($doctrine) {
		$this->doctrine = $doctrine;
	}
	
	public function getShopAverage(&$shop) {
		$builder = $this->doctrine->getRepository("AppBundle:ShopRatings")->createQueryBuilder("s");
		$query = $builder->select("avg(s.rating)")
						->groupBy("s.shop_id")
						->where ("s.shop_id=:shop_id")
						->setParameter('shop_id', $shop->getId())
						->getQuery();
		$result = $query->getResult();
		$average = (count($result) != 0) ? $result[0][1] : 0;
		$shop->setAverageRating($average);
	}
	
	public function getShopsAverage(&$shops) {
		
		foreach($shops as $shop) {
			$this->getShopAverage($shop);
		}
	}
}
