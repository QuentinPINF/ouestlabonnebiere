<?php
// src/AppBundle/Controller/NonRestController.php
namespace AppBundle\Controller;


use AppBundle\Entity\Beer;
use AppBundle\Entity\BeerPrices;
use AppBundle\Entity\BeerRatings;
use AppBundle\Entity\Tag;
use AppBundle\Entity\User;

use AppBundle\Form\BeerType;
use AppBundle\Form\BeerPricesType;
use AppBundle\Form\BeerRatingsType;
use AppBundle\Form\BeerTagType;
use AppBundle\Form\TagType;
use AppBundle\Form\UserType;

use AppBundle\Handler\BeerHandler;
use AppBundle\Handler\BeerPricesHandler;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NonRestController extends Controller
{
	public function getUser() {
		$userManager = $this->container->get('fos_user.user_manager');
		return $userManager->findUserByUsername($this->container->get('security.context')
                    ->getToken()
                    ->getUser());
	}
					
    /**
     * @Route("/login")
     */
	public function login() {
		return $this->getUser();
	}
	
    /**
     * @Route("/signup")
     */
	public function signup(Request $request) {
		$user = new User();
		$form = $this->createForm(new UserType(), $user);
		$form->handleRequest($request);
		if($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$user->setEnabled(true);
			$user->addRole("ROLE_USER");
			$user->addRole("ROLE_API");
			$em->persist($user);
			$em->flush();
			return $user;
		}
		return $form->getErrors();
	}
	
    /**
     * @Route("/search")
     */
	public function search(Request $request) {
		$search = $request->get("search");
		if($search == null)
			return new Response("No search", 400);
		return $this->getDoctrine()->getRepository("AppBundle:Beer")
				->createQueryBuilder("b")->where("b.name LIKE :search")->setParameter("search", "%$search%")
				->getQuery()->getResult();
	}
}

?>