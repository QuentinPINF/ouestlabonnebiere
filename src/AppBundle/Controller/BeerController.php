<?php
// src/AppBundle/Controller/BeerController.php
namespace AppBundle\Controller;

use AppBundle\Entity\Beer;
use AppBundle\Entity\BeerPrices;
use AppBundle\Entity\BeerRatings;
use AppBundle\Entity\Tag;

use AppBundle\Form\BeerType;
use AppBundle\Form\BeerPricesType;
use AppBundle\Form\BeerRatingsType;
use AppBundle\Form\BeerTagType;
use AppBundle\Form\TagType;

use AppBundle\Handler\BeerHandler;
use AppBundle\Handler\BeerPricesHandler;

use FOS\RestBundle\Controller\Annotations as FOSRestBundleAnnotations;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;

use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * @FOSRestBundleAnnotations\View()
 */
class BeerController extends BaseController
{
	public function __construct() {
		$this->entityRepository = "AppBundle:Beer";
	}
	
	/**********/
	/** Beer **/
	/**********/
	/*
	 * route GET /beers
	*/
    public function getBeersAction()
    {
		$beers = $this->doGetC();
		$beerHandler = new BeerHandler($this->getDoctrine());
		$beerHandler->getBeersAverage($beers);
		return $beers;
    }
	
	/*
	 * route GET /beers/{id}
	 */
	public function getBeerAction($id)
	{
		$beer = $this->doGet($id);
		$beerHandler = new BeerHandler($this->getDoctrine());
		$beerHandler->getBeerAverage($beer);
		return $beer;
	}
	
	/*
	 * route POST /beers
	 */
	public function postBeersAction(Request $req) {
		$beer = new Beer();
		$beerType = new BeerType();
		return $this->doPostC($req, $beer, $beerType);
	}
	
	/*
	 * route PUT /beers/{id}
	 */
	public function putBeersAction($id, Request $req) {
		$beer = $this->getBeerAction($id);
		$beerType = new BeerType();
		return $this->doPut($req, $beer, $beerType);
	}
	
	/**************/
	/** END Beer **/
	/**************/
	
	/***************/
	/** Beer Tags **/
	/***************/
	
	/*
	 * route GET /beers/{id}/tags
	 */
	public function getBeersTagsAction($id) {
		$beer = $this->getBeerAction($id);
		
		return $beer->getTags();
	}
	
	/*
	 * route POST /beers/{id}/tags
	 */
	public function postBeersTagsAction($id, Request $request) {
		$tag = new Tag();
		$tagType = new TagType();
		
		$beer = $this->getBeerAction($id);
		$tag->addBeer($beer);
		$beer->addTags($tag);
		
		return $this->doPostC($request, $tag, $tagType, false);
	}
	
	/*
	 * route PUT /beers/{id}/tags
	 */
	public function putBeersTagsAction($beerId, Request $req) {
		$beer = $this->getBeerAction($beerId);
		$btt = new BeerTagType();
		
		return $this->doPut($req, $beer, $btt, false);
	}
	
	
	/*******************/
	/** END Beer Tags **/
	/*******************/
	
	/*****************/
	/** Beer Rating **/
	/*****************/
	
	/*
	 * route GET /beers/{id}/ratings
	 */
	public function getBeersRatingsAction($id) {
		$beer = $this->getBeerAction($id);
		
		return $beer->getBeerRatings();
	}
	
	/*
	 * route POST /beers/{id}/ratings
	 */
	public function postBeersRatingsAction($id, Request $request) {
		$beerRatings = new BeerRatings();
		$beerRatingsType = new BeerRatingsType();
		
		$beer = $this->getBeerAction($id);
		$beerRatings->setBeer($beer);
		
		return $this->doPostComment($request, $beerRatings, $beerRatingsType);
	}
	
	/*********************/
	/** END Beer Rating **/
	/*********************/
	
	/*****************/
	/** Beer Prices **/
	/*****************/
	
	/*
	 * route GET /beers/{id}/prices
	 */
	public function getBeersPricesAction($id) {
		$beer = $this->getBeerAction($id);
		
		return $beer->getBeerPrices();
	}
	
	/*
	 * route POST /beers/{id}/prices
	 */
	public function postBeersPricesAction($id, Request $request) {
		$beerPrice = new BeerPrices();
		$beerPriceType = new BeerPricesType();
		
		$beer = $this->getBeerAction($id);
		$beerPrice->setBeer($beer);
		
		return $this->doPostC($request, $beerPrice, $beerPriceType, false);
	}
	
	/*
	 * route PUT /beers/{id}/prices/{beerPriceId}
	 */
	public function putBeersPricesAction($beerId, $beerPriceId, Request $request) {
		$bph = new BeerPricesHandler($this->getDoctrine());
		$beerPrice = $bph->get($beerPriceId);
		$beerPriceType = new BeerPricesType();
		
		$beer = $this->getBeerAction($beerId);
		$beerPrice->setBeer($beer);
		
		return $this->doPostC($request, $beerPrice, $beerPriceType, false);
	}
	
	/*********************/
	/** END Beer Prices **/
	/*********************/
	
	/**************************/
	/** Beer Recommendations **/
	/**************************/
	
	/*
	 * route GET /beers/{id}/recommendation
	 */
	public function getBeersRecommendationAction($beerId) {
		$beer = $this->doGet($beerId);
		$beers = array();
		//TODO: ajuster les coefficients
		$sql = <<<'EOT'
SELECT b.id, b.name, b.color_id, b.brewery_id, b.alcohol, b.description,

	(tags_in_common + common_brewery + common_color*2 + common_alcohol *.5) as score
FROM (
    SELECT b.id, b.name, b.color_id, b.brewery_id, b.alcohol, b.description,
        SUM(1) as tags_in_common,
        IF(brewery_id = :brewery_id, 1 ,0) as common_brewery,
        IF(color_id = :color_id, 1 ,0) as common_color,
        IF((alcohol > (:alcohol-1) AND alcohol < (:alcohol+1)), 1 ,0) as common_alcohol
    FROM `beer` AS b
    LEFT JOIN has_tags AS ht ON ht.beer_id = b.id
    WHERE validated = 1 AND id <> :beer_id AND (brewery_id = :brewery_id OR color_id = :color_id OR (alcohol > (:alcohol-1) AND alcohol < (:alcohol+1)) OR ht.tag_id in 		(SELECT tag_id FROM has_tags WHERE beer_id = :beer_id)) GROUP BY id
) as b
ORDER BY score DESC
LIMIT 5
EOT;
		$entityManager = $this->getDoctrine()->getManager();
		$rsmb = new ResultSetMappingBuilder($entityManager);
		$rsmb->addRootEntityFromClassMetadata(Beer::class, "b");
		//$rsmb->addScalarResult("score", "score");
		
		$query = $entityManager->createNativeQuery($sql, $rsmb);
		$query->setParameter("beer_id", $beer->getId());
		$query->setParameter("brewery_id", (int)$beer->getBrewery()->getId());
		$query->setParameter("color_id", $beer->getColor()->getId());
		$query->setParameter("alcohol", $beer->getAlcohol());
		
		return $query->getResult();
	}
	/******************************/
	/** END Beer Recommendations **/
	/******************************/
}

?>