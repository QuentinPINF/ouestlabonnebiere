<?php
// src/AppBundle/Controller/BaseController.php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as FOSRestBundleAnnotations;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * @FOSRestBundleAnnotations\View()
 */
class BaseController extends FOSRestController
{
	protected $entityRepository;
	
	public function getUser() {
		$userManager = $this->container->get('fos_user.user_manager');
		return $userManager->findUserByUsername($this->container->get('security.context')
                    ->getToken()
                    ->getUser());
	}
	
    public function doGetC()
    {
		$entities = $this->getDoctrine()
        ->getRepository($this->entityRepository)
        ->findBy(array("validated"=>true));
		
		return $entities;
    }
	
	public function doGet($id)
	{
		$entity = $this->getDoctrine()
        ->getRepository($this->entityRepository)
        ->find($id);
        return $entity;
	}
	
	public function doPostC(Request $req, $entity, $entityType, $validation=true) {
		$form = $this->createForm($entityType, $entity);
		$form->handleRequest($req);
		if($form->isValid()) {
			if($validation) {
				$user = $this->getUser();
				$entity->setValidated(in_array("ROLE_EDIT", $user->getRoles()));
			}
			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();
			return $entity;
		} else return $form->getErrors();
	}
	
	public function doPut(Request $req, $entity, $entityType, $validation=true) {
		$form = $this->createForm($entityType, $entity);
		$form->submit($req);
		var_dump($entity->getName());
		if($form->isValid()) {
			if($validation) {
				$user = $this->getUser();
				$editor = in_array("ROLE_EDIT", $user->getRoles());
				if($entity->getId() !== null && !$editor)
					return "You are not allowed to modify this.";
				$entity->setValidated($editor);
			}
			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();
			return $entity;
		} else return $form->getErrors();
	}
	
	public function doDelete($entity) {
		$user = $this->getUser();
		$editor = in_array("ROLE_EDIT", $user->getRoles());
		if($entity->getId() !== null && !$editor)
			return "You are not allowed to delete this.";
		
		$em = $this->getDoctrine()->getManager();
		$em->remove($entity);
		$em->flush();
		return $entity;
			
	}
	
	public function doPostComment(Request $req, $entity, $entityType) {
		$form = $this->createForm($entityType, $entity);
		$form->handleRequest($req);
		if($form->isValid()) {
			$user = $this->getUser();
			$entity->setUser($user);
			$entity->setDate(new \DateTime("now"));
			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();
			return $entity;
		} else return $form->getErrors();
	}
}

?>