<?php

namespace AppBundle\Controller;
use FOS\RestBundle\Controller\Annotations\Get;
use AppBundle\Entity\BeerRatings;
use AppBundle\Form\BeerRatingsType;

class BeerRatingsController extends BaseController
{
	public function getUserFromId($id) {
		return $this->getDoctrine()
        ->getRepository("AppBundle:User")
        ->find($id);
	}
	
    public function getBeer_ratingsAction($slug)
    {
		$user = $this->getUserFromId($slug);
		return $user->getBeerRatings();
	} // "get_user_BeerRatings"   [GET] /users/{slug}/BeerRatings
}

?>