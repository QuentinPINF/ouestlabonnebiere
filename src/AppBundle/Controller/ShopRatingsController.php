<?php

namespace AppBundle\Controller;
use FOS\RestBundle\Controller\Annotations\Get;
use AppBundle\Entity\ShopRatings;
use AppBundle\Form\ShopRatingsType;

class ShopRatingsController extends BaseController
{
	public function getUserFromId($id) {
		return $this->getDoctrine()
        ->getRepository("AppBundle:User")
        ->find($id);
	}
	
    public function getShop_ratingsAction($slug)
    {
		$user = $this->getUserFromId($slug);
		return $user->getShopRatings();
	}
}

?>