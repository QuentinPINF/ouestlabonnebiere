<?php
// src/AppBundle/Controller/UsersController.php
namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;

use FOS\RestBundle\Controller\Annotations as FOSRestBundleAnnotations;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * @FOSRestBundleAnnotations\View()
 */
class UsersController extends BaseController
{
	public function __construct() {
		
		$this->entityRepository = "AppBundle:User";
	}
	
    public function getUsersAction()
    {
		$users = $this->getDoctrine()
        ->getRepository($this->entityRepository)
        ->findAll();
		
		return $users;
    }
	
	public function getUserAction($id)
	{
		return $this->doGet($id);
	}
	
	public function postUserAction(Request $req) {
		$user = new User();
		$userType = new UserType();
		return $this->cpostAction($req, $user, $userType);
	}
	
}

?>