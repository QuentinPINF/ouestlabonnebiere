<?php
// src/AppBundle/Controller/TagController.php
namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use AppBundle\Form\TagType;

use FOS\RestBundle\Controller\Annotations as FOSRestBundleAnnotations;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * @FOSRestBundleAnnotations\View()
 */
class TagController extends BaseController
{
	public function __construct() {
		$this->entityRepository = "AppBundle:Tag";
	}
	
	/*********/
	/** Tag **/
	/*********/
	
	/*
	 * route GET /tags
	*/
    public function getTagsAction()
    {
		$tags = $this->doGetC();
		return $tags;
    }
	
	/*
	 * route GET /tags/{id}
	*/
	public function getTagAction($id)
	{
		$tag = $this->doGet($id);
		return $tag;
	}
	
	/*
	 * route POST /tags
	*/
	public function postTagsAction(Request $req) {
		$tag = new Tag();
		$tagType = new TagType();
		return $this->doPostC($req, $tag, $tagType);
	}
	
	/*
	 * route PUT /tags/{id}
	*/
	public function putTagsAction($id, Request $req) {
		$tag = $this->getTagAction($id);
		$tagType = new TagType();
		return $this->doPut($req, $tag, $tagType);
	}
	
	/*
	 * route DELETE /tags/{id}
	*/
	public function deleteTagsAction($id) {
		$tag = $this->getTagAction($id);
		return $this->doDelete($tag);
	}
	
	/*************/
	/** END Tag **/
	/*************/
	
	/*
	 * route GET /tags/{id}/beers
	*/
	public function getTagsBeersAction($id) {
		$tag = $this->getTagAction($id);
		return $tag->getBeers();
	}
	
}

?>