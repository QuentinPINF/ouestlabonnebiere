<?php
// src/AppBundle/Controller/ShopController.php
namespace AppBundle\Controller;

use AppBundle\Entity\Shop;
use AppBundle\Entity\ShopRatings;
use AppBundle\Form\BeerPricesType;
use AppBundle\Form\ShopType;
use AppBundle\Form\ShopRatingsType;
use AppBundle\Handler\ShopHandler;
use AppBundle\Handler\BeerPricesHandler;

use FOS\RestBundle\Controller\Annotations as FOSRestBundleAnnotations;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * @FOSRestBundleAnnotations\View()
 */
class ShopController extends BaseController
{
	public function __construct() {
		
		$this->entityRepository = "AppBundle:Shop";
	}
	
    public function getShopsAction()
    {
		$shops = $this->doGetC();
		$shopHandler = new ShopHandler($this->getDoctrine());
		$shopHandler->getShopsAverage($shops);
		return $shops;
    }
	
	public function getShopAction($id)
	{
		$shop = $this->doGet($id);
		$shopHandler = new ShopHandler($this->getDoctrine());
		$shopHandler->getShopAverage($shop);
		return $shop;
	}
	
	public function postShopsAction(Request $req) {
		$shop = new Shop();
		$shopType = new ShopType();
		return $this->doPostC($req, $shop, $shopType);
	}
	
	public function putShopsAction($id, Request $req) {
		$shop = $this->getShopAction($id);
		$shopType = new ShopType();
		return $this->doPut($req, $shop, $shopType);
	}
	
	public function getShopsRatingsAction($id) {
		$shop = $this->getShopAction($id);
		
		return $shop->getShopRatings();;
	}
	
	public function postShopsRatingsAction($id, Request $request) {
		$shopRatings = new ShopRatings();
		$shopRatingsType = new ShopRatingsType();
		
		$shop = $this->getShopAction($id);
		$shopRatings->setShop($shop);
		
		return $this->doPostComment($request, $shopRatings, $shopRatingsType);
	}
	
	
	/*****************/
	/** Shop Prices **/
	/*****************/
	
	/*
	 * route GET /shops/{id}/prices
	 */
	public function getShopsPricesAction($id) {
		$shop = $this->getShopAction($id);
		
		return $shop->getBeerPrices();
	}
	
	/*
	 * route POST /shops/{id}/prices
	 *//*
	public function postShopsPricesAction($id, Request $request) {
		$shopPrice = new ShopPrices();
		$shopPriceType = new ShopPricesType();
		
		$shop = $this->getShopAction($id);
		$shopPrice->setShop($shop);
		
		return $this->doPostC($request, $shopPrice, $shopPriceType, false);
	}*/
	
	/*
	 * route PUT /shops/{id}/prices/{shopPriceId}
	 */
	public function putShopsPricesAction($shopId, $shopPriceId, Request $request) {
		$bph = new BeerPricesHandler($this->getDoctrine());
		$shopPrice = $bph->get($shopPriceId);
		$shopPriceType = new BeerPricesType();
		
		$shop = $this->getShopAction($shopId);
		$shopPrice->setShop($shop);
		
		return $this->doPostC($request, $shopPrice, $shopPriceType, false);
	}
	
	/*********************/
	/** END Shop Prices **/
	/*********************/
}

?>