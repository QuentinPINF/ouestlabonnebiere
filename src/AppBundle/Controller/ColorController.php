<?php
// src/AppBundle/Controller/ColorController.php
namespace AppBundle\Controller;

use AppBundle\Entity\Color;
use AppBundle\Form\ColorType;

use FOS\RestBundle\Controller\Annotations as FOSRestBundleAnnotations;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @FOSRestBundleAnnotations\View()
 */
class ColorController extends BaseController
{
	public function __construct() {
		$this->entityRepository = "AppBundle:Color";
	}
	
	/***********/
	/** Color **/
	/***********/
	
	/**
	 * Get all colors
	 * @ApiDoc(
	 *  section="Color",
	 *  description="Get a color",
	 *  input="int",
	 *  output="AppBundle\Entity\Color[]",
	 *  statusCodes={
	 *         200="Returned when successful"
	 *  }
	 * )
	 */
    public function getColorsAction()
    {
		$colors = $this->doGetC();
		return $colors;
    }
	
	/**
	 * Get a color
	 * @ApiDoc(
	 *  section="Color",
	 *  description="Get a color",
	 *  input="int",
	 *  output="AppBundle\Entity\Color",
	 *  statusCodes={
	 *         200="Returned when successful"
	 *  }
	 * )
	 */
	public function getColorAction($id)
	{
		$color = $this->doGet($id);
		return $color;
	}
	
	/**
	 * Create a new color
	 * @ApiDoc(
	 *  section="Color",
	 *  description="Create a new Color",
	 *  input="AppBundle\Form\ColorType",
	 *  output="AppBundle\Entity\Color",
	 *  statusCodes={
	 *         200="Returned when successful"
	 *  }
	 * )
	 */
	public function postColorsAction(Request $req) {
		$color = new Color();
		$colorType = new ColorType();
		return $this->doPostC($req, $color, $colorType);
	}
	
	/**
	 * Update an existing color
	 * @ApiDoc(
	 *  section="Color",
	 *  description="Update an existing color",
	 *  input="AppBundle\Form\ColorType",
	 *  output="AppBundle\Entity\Color",
	 *  statusCodes={
	 *         200="Returned when successful"
	 *  }
	 * )
	 */
	public function putColorsAction($id, Request $req) {
		$color = $this->getColorAction($id);
		$colorType = new ColorType();
		return $this->doPut($req, $color, $colorType);
	}
	
	/**
	 * Delete an existing color
	 * @ApiDoc(
	 *  section="Color",
	 *  description="Delete an existing color",
	 *  input="AppBundle\Form\ColorType",
	 *  output="AppBundle\Entity\Color",
	 *  statusCodes={
	 *         200="Returned when successful"
	 *  }
	 * )
	 */
	public function deleteColorsAction($id) {
		$color = $this->getColorAction($id);
		return $this->doDelete($color);
	}
	
	/*****************/
	/** END Color **/
	/*****************/
	
	/*
	 * route GET /colors/{id}/beers
	*/
	public function getColorsBeersAction($id) {
		$color = $this->getColorAction($id);
		return $color->getBeers();
	}
	
}

?>