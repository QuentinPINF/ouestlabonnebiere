<?php
// src/AppBundle/Controller/BreweryController.php
namespace AppBundle\Controller;

use AppBundle\Entity\Brewery;
use AppBundle\Form\BreweryType;

use FOS\RestBundle\Controller\Annotations as FOSRestBundleAnnotations;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * @FOSRestBundleAnnotations\View()
 */
class BreweryController extends BaseController
{
	public function __construct() {
		$this->entityRepository = "AppBundle:Brewery";
	}
	
	/*************/
	/** Brewery **/
	/*************/
	
	/*
	 * route GET /breweries
	*/
    public function getBreweriesAction()
    {
		$breweries = $this->doGetC();
		return $breweries;
    }
	
	/*
	 * route GET /breweries/{id}
	*/
	public function getBreweryAction($id)
	{
		$brewery = $this->doGet($id);
		return $brewery;
	}
	
	/*
	 * route POST /breweries
	*/
	public function postBreweriesAction(Request $req) {
		$brewery = new Brewery();
		$breweryType = new BreweryType();
		return $this->doPostC($req, $brewery, $breweryType);
	}
	
	/*
	 * route PUT /breweries/{id}
	*/
	public function putBreweriesAction($id, Request $req) {
		$brewery = $this->getBreweryAction($id);
		$breweryType = new BreweryType();
		return $this->doPut($req, $brewery, $breweryType);
	}
	
	/*
	 * route DELETE /breweries/{id}
	*/
	public function deleteBreweriesAction($id) {
		$brewery = $this->getBreweryAction($id);
		return $this->doDelete($brewery);
	}
	
	/*****************/
	/** END Brewery **/
	/*****************/
	
	/*
	 * route GET /breweries/{id}/beers
	*/
	public function getBreweriesBeersAction($id) {
		$brewery = $this->getBreweryAction($id);
		return $brewery->getBeers();
	}
	
}

?>