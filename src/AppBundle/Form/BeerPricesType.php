<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BeerPricesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price', null, array("constraints"=>array(new NotBlank())))
            ->add('volume', null, array("constraints"=>array(new NotBlank())))
            ->add('draught', null, array("constraints"=>array(new NotBlank())))
            ->add('deposit', null, array("constraints"=>array(new NotBlank())))
            ->add('shop', EntityType::class, array("constraints"=>array(new NotBlank()), "class"=>"AppBundle:Shop"))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\BeerPrices'
        ));
    }
	
	public function getName() {
		return "";
	}
}
