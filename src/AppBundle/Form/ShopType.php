<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class ShopType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array("constraints"=>array(new NotBlank())))
            ->add('latitude', null,  array("constraints"=>array(new NotBlank())))
            ->add('longitude', null,  array("constraints"=>array(new NotBlank())))
            ->add('description', null,  array("constraints"=>array(new NotBlank())))
            ->add('address', null,  array("constraints"=>array(new NotBlank())))
            ->add('validated')
            //->add('user')
            //->add('beer')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Shop'
        ));
    }
	
	public function getName() {
		return "";
	}
}
