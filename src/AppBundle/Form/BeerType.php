<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class BeerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array("constraints"=>array(new NotBlank())))
            ->add('alcohol', null, array("constraints"=>array(new NotBlank())))
            ->add('description', null, array("constraints"=>array(new NotBlank())))
            ->add('validated')
            ->add('color', null, array("constraints"=>array(new NotBlank())))
            ->add('brewery', null, array("constraints"=>array(new NotBlank())))
            ->add('tags')
            //->add('shop')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Beer'
        ));
    }
	
	public function getName() {
		return "";
	}
}
